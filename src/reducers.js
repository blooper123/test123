import {
  UPDATE_TICKER,
  CHANGE_TRADING_PAIR,
  ADD_TRADE,
  SET_ORDERBOOK,
  DELETE_ORDERBOOK_BID,
  DELETE_ORDERBOOK_ASK,
  UPDATE_ORDERBOOK_BID,
  UPDATE_ORDERBOOK_ASK,
  UPDATE_CONNECTION_STATUS,
  SET_PRECISION
} from './actions';


/**
 * App constants
 */
const MAX_NUM_TRADES_VISIBLE              = 18;

export const CONNECTION_STATUS_OFFLINE    = 'offline';
export const CONNECTION_STATUS_CONNECTING = 'connecting';
export const CONNECTION_STATUS_CONNECTED  = 'connected';
export const CONNECTION_STATUS_ERROR      = 'error';

export const MIN_PRECISION                = 0;
export const MAX_PRECISION                = 4;


/**
 * Initial app state
 */
const initialState = {
  tradingPair: 'BTCUSD',
  ticker: null,
  trades: [],
  book:   {
    bids: [],
    asks: []
  },
  precision: 2,
  connectionStatus: CONNECTION_STATUS_OFFLINE
};

/**
 * Reducer for ticker actions.
 */
export function ticker(state = initialState.ticker, action) {
  if (UPDATE_TICKER === action.type) {
    return Object.assign({}, state, {
      lastPrice:          action.lastPrice,
      dailyVolume:        action.dailyVolume,
      dailyChange:        action.dailyChange,
      dailyChangePercent: action.dailyChangePercent,
      dailyLow:           action.dailyLow,
      dailyHigh:          action.dailyHigh,
    });
  }

  return state;
}

/**
 * Reducer for connection status actions.
 */
export function connectionStatus(state = initialState.connectionStatus, action) {
  if (UPDATE_CONNECTION_STATUS === action.type) {
    return action.connectionStatus;
  }

  return state;
}

/**
 * Reducer for trading pair actions.
 */
export function tradingPair(state = initialState.tradingPair, action) {
  if (CHANGE_TRADING_PAIR === action.type) {
    return action.tradingPair;
  }

  return state;
}

/**
 * Reducer for precision actions.
 */
export function precision(state = initialState.precision, action) {
  if (SET_PRECISION === action.type) {
    return action.precision;
  }

  return state;
}

/**
 * Reducer for trade list actions.
 */
export function trades(state = initialState.trades, action) {
  if (ADD_TRADE === action.type) {
    return [
      {
        timestamp: action.timestamp,
        amount:    action.amount,
        price:     action.price
      },
      ...state
    ].slice(0, MAX_NUM_TRADES_VISIBLE);
  }

  return state;
}

/**
 * Reducer for orderbook actions.
 */
export function book(state = initialState.book, action) {
  switch (action.type) {
    case SET_ORDERBOOK:
      return {
        bids: action.orders.filter(order => order.amount > 0),
        asks: action.orders.filter(order => order.amount < 0)
      };

    case DELETE_ORDERBOOK_BID:
      return {
        asks: state.asks,
        bids: state.bids.filter(order => order.price !== action.price)
      };

    case DELETE_ORDERBOOK_ASK:
      return {
        asks: state.asks.filter(order => order.price !== action.price),
        bids: state.bids
      };

    case UPDATE_ORDERBOOK_BID:
    {
      let bids   = state.bids;
      let {tmp, ...order} = action;

      if (bids.find(bid => bid.price === action.price)) {
        // If price level already exists, update fields.
        bids = bids.map(bid => bid.price === order.price ? order : bid);
      } else {
        // If not, add a new one.
        bids.push(order);
      }

      return {
        asks: state.asks,
        bids: bids
      };
    }

    case UPDATE_ORDERBOOK_ASK:
    {
      let asks   = state.asks;
      let {tmp, ...order} = action;

      if (asks.find(ask => ask.price === action.price)) {
        // If price level already exists, update fields.
        asks = asks.map(ask => ask.price === order.price ? order : ask);
      } else {
        // If not, add a new one.
        asks.push(order);
      }

      return {
        asks: asks,
        bids: state.bids
      };
    }

    default:
      return state;
  }
}