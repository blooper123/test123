function makeActionCreator(type, ...argNames) {
  return function (...args) {
    const action = { type };
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index];
    });
    return action;
  }
}

/**
 * Action types
 */
export const UPDATE_TICKER             = 'UPDATE_TICKER';
export const CHANGE_TRADING_PAIR       = 'CHANGE_TRADING_PAIR';
export const ADD_TRADE                 = 'ADD_TRADE';
export const SET_ORDERBOOK             = 'SET_ORDERBOOK';
export const DELETE_ORDERBOOK_BID      = 'DELETE_ORDERBOOK_BID';
export const DELETE_ORDERBOOK_ASK      = 'DELETE_ORDERBOOK_ASK';
export const UPDATE_ORDERBOOK_BID      = 'UPDATE_ORDERBOOK_BID';
export const UPDATE_ORDERBOOK_ASK      = 'UPDATE_ORDERBOOK_ASK';
export const UPDATE_CONNECTION_STATUS  = 'UPDATE_CONNECTION_STATUS';
export const SET_PRECISION             = 'SET_PRECISION';

/**
 * Action creators
 */
export const updateTicker           = makeActionCreator(UPDATE_TICKER, 'lastPrice', 'dailyVolume', 'dailyChange', 'dailyChangePercent', 'dailyLow', 'dailyHigh');
export const addTrade               = makeActionCreator(ADD_TRADE, 'timestamp', 'price', 'amount');
export const setOrderBook           = makeActionCreator(SET_ORDERBOOK, 'orders');
export const deleteOrderBookBid     = makeActionCreator(DELETE_ORDERBOOK_BID, 'price');
export const deleteOrderBookAsk     = makeActionCreator(DELETE_ORDERBOOK_ASK, 'price');
export const updateOrderBookBid     = makeActionCreator(UPDATE_ORDERBOOK_BID, 'price', 'count', 'amount');
export const updateOrderBookAsk     = makeActionCreator(UPDATE_ORDERBOOK_ASK, 'price', 'count', 'amount');
export const updateConnectionStatus = makeActionCreator(UPDATE_CONNECTION_STATUS, 'connectionStatus');
export const setPrecision           = makeActionCreator(SET_PRECISION, 'precision');
