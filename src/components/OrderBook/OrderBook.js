import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import OrderBookList from '../OrderBookList/OrderBookList';
import { setPrecision } from '../../actions';
import { MIN_PRECISION, MAX_PRECISION } from '../../reducers';
import './OrderBook.scss';

class OrderBook extends Component {

  increasePrecision() {
    this.props.onSetPrecision(Math.min(this.props.precision + 1, MAX_PRECISION));
  }
  decreasePrecision() {
    this.props.onSetPrecision(Math.max(this.props.precision - 1, MIN_PRECISION));
  }

  render() {
    return (
      <div className="OrderBook">
        <h3 className="OrderBook-Title">
          Order Book <span>{this.props.tradingPair}</span>

          <button onClick={() => this.increasePrecision()} title="Increase Precision" disabled={this.props.precision === MAX_PRECISION}>+</button>
          <button onClick={() => this.decreasePrecision()} title="Decrease Precision" disabled={this.props.precision === MIN_PRECISION}>-</button>
        </h3>
        <div className="OrderBook-Lists">
          <OrderBookList orders={this.props.bids} type="bids" key="bids" />
          <OrderBookList orders={this.props.asks} type="asks" key="asks" />
        </div>
      </div>
    );
  }
}

OrderBook.propTypes = {
  tradingPair: PropTypes.string.isRequired,
  bids:   PropTypes.arrayOf(PropTypes.shape({
    count:     PropTypes.number.isRequired,
    amount:    PropTypes.number.isRequired,
    price:     PropTypes.number.isRequired
  })).isRequired,
  asks:  PropTypes.arrayOf(PropTypes.shape({
    count:     PropTypes.number.isRequired,
    amount:    PropTypes.number.isRequired,
    price:     PropTypes.number.isRequired
  })).isRequired,
  onSetPrecision: PropTypes.func,
  precision: PropTypes.number.isRequired
};

const mapStateToProps = state => {
  return {
    tradingPair: state.tradingPair,
    precision:   state.precision,
    bids:        state.book.bids,
    asks:        state.book.asks
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSetPrecision: (precision) => dispatch(setPrecision(precision)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderBook);
