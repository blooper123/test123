import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import FormattedNumber from '../FormattedNumber/FormattedNumber';
import './OrderBookItem.scss';

class OrderBookItem extends PureComponent {
  render() {
    const action = this.props.amount < 0 ? 'sell' : 'buy';

    let rowContents = [
      this.props.count,
      <FormattedNumber numDecimals={1}>{Math.abs(this.props.amount)}</FormattedNumber>,
      <FormattedNumber numDecimals={2}>{Math.abs(this.props.total)}</FormattedNumber>,
      <FormattedNumber numDecimals={1}>{this.props.price}</FormattedNumber>
    ];

    // The sales table appears with the columns in reverse order.
    if (action === 'sell') {
      rowContents = rowContents.reverse();
    }

    return (
      <tr className={`OrderBookItem OrderBookItem--${action}`}>
        { rowContents.map((content, index) => <td key={index}>{content}</td>) }
      </tr>
    );
  }
}

OrderBookItem.propTypes = {
  count:     PropTypes.number.isRequired,
  amount:    PropTypes.number.isRequired,
  price:     PropTypes.number.isRequired,
  total:     PropTypes.number.isRequired
};

export default OrderBookItem;
