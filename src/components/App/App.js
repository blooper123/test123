import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Ticker from '../Ticker/Ticker';
import TradeList from '../TradeList/TradeList';
import OrderBook from '../OrderBook/OrderBook';
import Connection from '../Connection/Connection';
import './App.scss';

class App extends PureComponent {
  render() {
    return (
      <div className="App">
        <h1 className="App-Title">
          µDashboard
          <Connection />
        </h1>
        <div className="App-Columns">
          <div className="App-Column">
            { this.props.showTicker && <Ticker/> }
            { this.props.showTrades && <TradeList/> }
          </div>
          <div className="App-Column">
            { this.props.showOrderBook && <OrderBook/> }
          </div>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  showTicker:    PropTypes.bool.isRequired,
  showTrades:    PropTypes.bool.isRequired,
  showOrderBook: PropTypes.bool.isRequired,
};

const mapStateToProps = state => {
  return {
    showTicker:    state.ticker !== null,
    showTrades:    state.trades.length > 0,
    showOrderBook: state.book.bids.length > 0 || state.book.asks.length > 0
  };
};

export default connect(mapStateToProps)(App);

