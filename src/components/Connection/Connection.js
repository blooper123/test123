import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  updateTicker,
  addTrade,
  setOrderBook,
  deleteOrderBookBid,
  deleteOrderBookAsk,
  updateOrderBookBid,
  updateOrderBookAsk,
  updateConnectionStatus
} from '../../actions';
import {
  CONNECTION_STATUS_OFFLINE,
  CONNECTION_STATUS_CONNECTING,
  CONNECTION_STATUS_CONNECTED,
  CONNECTION_STATUS_ERROR
} from '../../reducers';
import './Connection.scss';

class Connection extends Component {
  constructor(props) {
    super(props);
    this.startConnection();
  }

  startConnection() {
    this.channelIds = {
      ticker: null,
      trades: null,
      book:   null,
    };
    this.channelNames = {};
    this.socket = new WebSocket('wss://api.bitfinex.com/ws/2');
    this.socket.addEventListener('open',    (ev) => this.onOpen(ev));
    this.socket.addEventListener('message', (ev) => this.onMessage(ev));
    this.socket.addEventListener('close',   (ev) => this.onClose(ev));
    this.props.onUpdateConnectionStatus(CONNECTION_STATUS_CONNECTING);
  }

  closeConnection() {
    this.socket.close();
  }

  onOpen(event) {
    for (let channel of Object.keys(this.channelIds)) {
      this.subscribeToChannel(channel);
    }
    this.props.onUpdateConnectionStatus(CONNECTION_STATUS_CONNECTED);
  }

  subscribeToChannel(channel) {
    let payload = {
      event:   'subscribe',
      channel: channel,
      symbol:  't' + this.props.tradingPair
    };
    if (channel === 'book') {
      payload.prec = 'P' + this.props.precision;
    }
    this.socket.send(JSON.stringify(payload));
  }

  onClose(event) {
    this.props.onUpdateConnectionStatus(CONNECTION_STATUS_OFFLINE);
  }

  onMessage(event) {
    if (!event.data) {
      return false;
    }

    const data = JSON.parse(event.data);

    // If one of them handles the message, we stop.
    if (this.handleChannelSubscriptions(data)) {
      return;
    }

    if (this.handleHeartbeats(data)) {
      return;
    }

    if (this.handleTickerData(data)) {
      return;
    }

    if (this.handleTradesData(data)) {
      return;
    }

    if (this.handleOrderBookData(data)) {
      return;
    }
  }

  handleHeartbeats(data) {
    return data[1] === 'hb';
  }

  handleChannelSubscriptions(data) {
    if (data.event) {
      switch (data.event) {
        case 'subscribed':
          this.channelIds[data.channel]  = data.chanId;
          this.channelNames[data.chanId] = data.channel;
          return true;
        case 'unsubscribed':
          const channel = this.channelNames[data.chanId];
          delete this.channelIds[channel];
          this.channelNames[data.chanId] = null;

          // resubscribe
          this.subscribeToChannel(channel);
          return true;
        case 'info':
          // Reconnect / Entering Maintenance mode
          if (data.code === 20051 || data.code === 20060) {
            this.stopConnection();
            if (data === 20051) {
              this.startConnection();
            }
          }
          return true;
        case 'error':
          console.error("Connection error: ", data.code, data.msg);
          this.props.onUpdateConnectionStatus(CONNECTION_STATUS_ERROR);
          return true;
        default:
          return true;
      }
    }

    return false;
  }

  handleTickerData(data) {
    if (this.channelIds.ticker !== data[0]) {
      return false;
    }
    this.props.onUpdateTicker(
      data[1][6], // lastPrice
      data[1][7], // dailyVolume
      data[1][4], // dailyChange
      data[1][5] * 100, // dailyChangePercent
      data[1][9], // dailyLow
      data[1][8], // dailyHigh
    );

    return true;
  }

  handleTradesData(data) {
    if (this.channelIds.trades !== data[0]) {
      return false;
    }

    // Is it a snapshot?
    if (Array.isArray(data[1])) {
      // We reverse it since they arrive oldest first.
      data[1].reverse().forEach(trade => {
        this.props.onAddTrade(
          trade[1], // timestamp
          trade[3], // price
          trade[2], // amount
        );
      });
    }

    // New trade?
    if (data[1] === 'te') {
      this.props.onAddTrade(
        data[2][1], // timestamp
        data[2][3], // price
        data[2][2], // amount
      );
    }

    return true;
  }

  handleOrderBookData(data) {
    if (this.channelIds.book !== data[0]) {
      return false;
    }

    // Is it a snapshot?
    if (Array.isArray(data[1]) && Array.isArray(data[1][0])) {
      this.props.onSetOrderBook(data[1].map(item => {
        return {
          price:  item[0],
          count:  item[1],
          amount: item[2]
        }
      }));
      return true;
    }

    var price, count, amount;
    [price, count, amount] = data[1];

    // Update order book if it's just a single order.
    if (count === 0) {
      this.props.onDeleteOrderBookItem(price, amount);
    }
    else {
      this.props.onUpdateOrderBookItem(price, count, amount);
    }

    return true;
  }

  toggleConnectionStatus() {
    switch (this.props.connectionStatus) {
      case CONNECTION_STATUS_CONNECTED:
      case CONNECTION_STATUS_CONNECTING:
        this.closeConnection();
        break;

      default:
        this.startConnection();
        break;
    }
  }

  componentDidUpdate(prevProps) {
    // Resubscribe to book channel with new precision?
    if (prevProps.precision !== this.props.precision) {
      this.socket.send(JSON.stringify({
        event:  'unsubscribe',
        chanId: this.channelIds.book
      }));
    }
  }

  render() {
    let linkLabel;

    switch (this.props.connectionStatus) {
      case CONNECTION_STATUS_CONNECTED:
        linkLabel  = 'Disconnect';
        break;

      case CONNECTION_STATUS_CONNECTING:
        linkLabel  = 'Abort';
        break;

      case CONNECTION_STATUS_OFFLINE:
        linkLabel  = 'Reconnect';
        break;

      case CONNECTION_STATUS_ERROR:
        linkLabel  = 'Retry';
        break;

      default:
        return null;
    }

    return (
      <div className={`Connection Connection--${this.props.connectionStatus}`}>
        {this.props.connectionStatus}
        <button onClick={() => this.toggleConnectionStatus()}>{linkLabel}</button>
      </div>
    );
  }
}

Connection.propTypes = {
  tradingPair:    PropTypes.string.isRequired,
  connectionStatus: PropTypes.string.isRequired,
  onUpdateTicker: PropTypes.func.isRequired,
  onAddTrade:     PropTypes.func.isRequired,
  onSetOrderBook: PropTypes.func.isRequired,
  onDeleteOrderBookItem: PropTypes.func.isRequired,
  onUpdateOrderBookItem: PropTypes.func.isRequired,
  onUpdateConnectionStatus: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    tradingPair:      state.tradingPair,
    connectionStatus: state.connectionStatus,
    precision:        state.precision
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateTicker: (lastPrice, dailyVolume, dailyChange, dailyChangePercent, dailyLow, dailyHigh) => {
      dispatch(updateTicker(lastPrice, dailyVolume, dailyChange, dailyChangePercent, dailyLow, dailyHigh))
    },
    onAddTrade: (timestamp, price, amount) => {
      dispatch(addTrade(timestamp, price, amount))
    },
    onSetOrderBook: (orders) => {
      dispatch(setOrderBook(orders))
    },
    onDeleteOrderBookItem: (price, amount) => {
      dispatch(amount > 0 ? deleteOrderBookBid(price) : deleteOrderBookAsk(price))
    },
    onUpdateOrderBookItem: (price, count, amount) => {
      dispatch(amount > 0 ? updateOrderBookBid(price, count, amount) : updateOrderBookAsk(price, count, amount))
    },
    onUpdateConnectionStatus: (connectionStatus) => {
      dispatch(updateConnectionStatus(connectionStatus))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Connection);

