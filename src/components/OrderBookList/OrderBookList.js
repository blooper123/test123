import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import OrderBookItem from '../OrderBookItem/OrderBookItem';
import './OrderBookList.scss';

class OrderBookList extends PureComponent {
  render() {
    let columnLabels = ['Count', 'Amount', 'Total', 'Price'];
    let orders       = this.props.orders;

    // The sales table appears with the columns in reverse order.
    if (this.props.type === 'asks') {
      columnLabels = columnLabels.reverse();
      orders.sort((a, b) => a.price - b.price);
    } else {
      orders.sort((a, b) => b.price - a.price);
    }

    let accum = 0;
    orders.map(order => {
      accum      += order.amount;
      order.total = accum;
      return order;
    });

    return (
      <div className="OrderBookList">
        <table className="OrderBookList-Data">
          <thead>
            <tr>
              { columnLabels.map((label, index) => <th key={index}>{label}</th>) }
            </tr>
          </thead>
          <tbody>
            { this.props.orders.map((order, index) => <OrderBookItem {...order} key={index} /> ) }
          </tbody>
        </table>
      </div>
    );
  }
}

OrderBookList.propTypes = {
  orders:  PropTypes.arrayOf(PropTypes.shape({
    count:     PropTypes.number.isRequired,
    amount:    PropTypes.number.isRequired,
    price:     PropTypes.number.isRequired
  })).isRequired,
  type:    PropTypes.oneOf(['bids', 'asks'])
};

export default OrderBookList;
