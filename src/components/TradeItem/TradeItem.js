import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FormattedNumber from '../FormattedNumber/FormattedNumber';
import './TradeItem.scss';

class TradeItem extends Component {
  getTradeTime() {
    const date = new Date(this.props.timestamp);

    return ('0'+date.getHours()).slice(-2)   + ':' +
           ('0'+date.getMinutes()).slice(-2) + ':' +
           ('0'+date.getSeconds()).slice(-2);
  }

  render() {
    const action = this.props.amount < 0 ? 'sell' : 'buy';

    return (
      <tr className={`TradeItem TradeItem--${action}`}>
        <td>⌃</td>
        <td>{this.getTradeTime()}</td>
        <td>
          <FormattedNumber numDecimals={1}>{this.props.price}</FormattedNumber>
        </td>
        <td>
          <FormattedNumber numDecimals={4}>{Math.abs(this.props.amount)}</FormattedNumber>
        </td>
      </tr>
    );
  }
}

TradeItem.propTypes = {
  timestamp: PropTypes.number.isRequired,
  amount:    PropTypes.number.isRequired,
  price:     PropTypes.number.isRequired
};

export default TradeItem;
