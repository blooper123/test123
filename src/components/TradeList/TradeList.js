import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TradeItem from '../TradeItem/TradeItem';
import './TradeList.scss';

class TradeList extends Component {
  render() {
    return (
      <div className="TradeList">
        <h3 className="TradeList-Title">Trades <span>{this.props.tradingPair}</span></h3>
        <table className="TradeList-Data">
            <thead>
                <tr>
                    <th></th>
                    <th>Time</th>
                    <th>Price</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                { this.props.trades.map((trade, index) => <TradeItem {...trade} key={index} /> ) }
            </tbody>
        </table>
      </div>
    );
  }
}

TradeList.propTypes = {
  tradingPair: PropTypes.string.isRequired,
  trades:      PropTypes.arrayOf(PropTypes.shape({
    timestamp: PropTypes.number.isRequired,
    amount:    PropTypes.number.isRequired,
    price:     PropTypes.number.isRequired
  })).isRequired
};

const mapStateToProps = state => {
  return {
    tradingPair: state.tradingPair,
    trades:      state.trades
  };
};

export default connect(mapStateToProps)(TradeList);
