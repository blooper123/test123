import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import FormattedNumber from '../FormattedNumber/FormattedNumber';
import './Ticker.scss';

class Ticker extends PureComponent {
  render() {
    const dailyTrend = this.props.dailyChange < 0 ? 'down' : 'up';

    return (
      <div className="Ticker">
        <div className="Ticker-Pair">
          {this.props.tradingPair}
        </div>
        <div className="Ticker-Last-Price">
          <FormattedNumber numDecimals={1}>{this.props.lastPrice}</FormattedNumber>
        </div>
        <div className="Ticker-Daily-Volume">
          Vol <FormattedNumber numDecimals={0}>{this.props.dailyVolume}</FormattedNumber> USD
        </div>
        <div className={`Ticker-Daily-Change Ticker-Daily-Change--${dailyTrend}`}>
          <FormattedNumber>{Math.abs(this.props.dailyChange)}</FormattedNumber>
          (<FormattedNumber>{Math.abs(this.props.dailyChangePercent)}</FormattedNumber>%)
        </div>
        <div className="Ticker-Daily-Low">
          Low <FormattedNumber numDecimals={1}>{this.props.dailyLow}</FormattedNumber>
        </div>
        <div className="Ticker-Daily-High">
          High <FormattedNumber numDecimals={1}>{this.props.dailyHigh}</FormattedNumber>
        </div>
      </div>
    );
  }
}

Ticker.propTypes = {
  tradingPair:        PropTypes.string.isRequired,
  lastPrice:          PropTypes.number.isRequired,
  dailyVolume:        PropTypes.number.isRequired,
  dailyChange:        PropTypes.number.isRequired,
  dailyChangePercent: PropTypes.number.isRequired,
  dailyLow:           PropTypes.number.isRequired,
  dailyHigh:          PropTypes.number.isRequired
};

const mapStateToProps = state => {
  return {
    tradingPair: state.tradingPair,
    ...state.ticker
  };
};

export default connect(mapStateToProps)(Ticker);
