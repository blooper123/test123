import React from 'react';
import PropTypes from 'prop-types';

const FormattedNumber = props => {
  const text = props.children.toLocaleString(navigator.language, {
    minimumFractionDigits: props.numDecimals || 2,
    maximumFractionDigits: props.numDecimals || 2
  });

  return <span>{text}</span>;
};

FormattedNumber.propTypes = {
  numDecimals: PropTypes.number
};

export default FormattedNumber;
